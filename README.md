# Envelope Generator

Eurorack envelope generator.

Based roughly on https://www.reddit.com/r/synthdiy/comments/llymt2/envelope_generator_ar_in_eurorack_format_on/
and
Moritz Klein's ADSR design: https://www.youtube.com/watch?v=aGFb7JbTdNU
